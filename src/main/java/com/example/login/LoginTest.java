package com.example.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginTest {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get("https://passport.yandex.ru/auth");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.inputLogin("test@m.com");
        loginPage.clickLoginBtn();
        loginPage.inputPasswd("qwerty12345");
    }
}
